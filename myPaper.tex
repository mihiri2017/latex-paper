\documentclass[a4paper,11pt]{article}

\usepackage{amssymb, amsmath}
\usepackage{graphicx}
\usepackage{listings}
\usepackage{multicol}
\usepackage{geometry}
\geometry{
 a4paper,
 total={170mm,257mm},
 left=20mm,
 top=20mm,
 }
\usepackage{hyperref}
\urlstyle{same}

\usepackage{fancyhdr}
\pagestyle{fancy}
\renewcommand{\headrulewidth}{0pt}

\hypersetup{
    colorlinks=true,
    urlcolor=black,
}

\date{}

\providecommand{\keywords}{\textbf{Keywords:} }


\title{\textbf{Fundamental Methods of Numerical \\ Extrapolation \\ with Applications}}
\author{Eric Hung-Lin Liu \\ \href{mailto:ehliu@mit.edu}{ehliu@mit.edu}}
\begin{document}
\maketitle
\thispagestyle{fancy}
\lhead{\textit {Fundamental Methods of Extrapolation}}
\rhead{\thepage}
\fancyfoot[L,R,C]{}

\keywords{numerical analysis, extrapolation, richardson, romberg, numerical differentiation, numerical integration}

\begin{abstract}

Extrapolation is an incredibly powerful technique for increasing speed and accuracy
in various numerical tasks in scientific computing. As we will see, extrapolation can
transform even the most mundane of algorithms (such as the Trapezoid Rule) into an
extremely fast and accurate algorithm, increasing the rate of convergence by more than
one order of magnitude. Within this paper, we will first present some background theory to
motivate the derivation of Richardson and Romberg Extrapolation and provide support for
their validity and stability as numerical techniques. Then we will present examples from
differentiation and integration to demonstrate the incredible power of extrapolation. We
will also give some MATLAB code to show some simple implentation methods in addition
to discussing error bounds and differentiating between ”‘true”’ and ”‘false”’ convergence. \end{abstract}

\section{Introduction}
In many practical applications, we often find ourselves attempting to compute continuous quan-
tities on discrete machines of limited precision. In several classic applications (e.g. numerical
differentiation), this is accomplished by tending the step-size to zero. Unfortunately as we will
see, this creates severe problems with floating point round-off errors. In other situations such
as numerical integration, tending the step-size toward zero does not cause extreme round-off
errors, but it can be severely limiting in terms of execution time: a decrease by a factor of 10
increases the required function evaluations by a factor of 10 \cite{NumericalAnalysis}. 

Extrapolation, the topic of this paper, attempts to rectify these problems by computing
”weighted averages” of relatively poor estimates to eliminate error modes–specifically, we will
be examining various forms of Richardson Extrapolation. Richardson and Richardson-like
schemes are but a subset of all extrapolation methods, but they are among the most common.
Moreover they have numerous applications and are not terribly difficult to understand and im-
plement. We will demonstrate that various numerical methods taught in introductory calculus
classes are in fact insufficient, but they can all be greatly improved through proper application
of extrapolation techniques.

\subsection{Background: A Few Words on Series Expansions}

We are very often able to associate some infinite sequence \(A_n\) with a known function \(A(h)\).
For example, \(A(h)\) may represent a first divided differences scheme for differentiation, where \(h\)
is the step-size. Note that h may be continuous or discrete. The sequence \({A_n}\) is described by:
\(A_n = A(h_n), n \in \mathbb{N} \) for a monotonically decreasing sequence \({h_n}\) that converges to 0 in the
limit. Thus \(lim_{h\to 0_+}A(h) = A \)and similarly, \(lim_{h\to\infty}A_n = A\). Computing \(A(0)\) is exactly what
we want to do–continuing the differentiation example, roughly speaking \(A(0)\) is the ”point”
where the divided differences ”becomes” the derivative. \cite{knuthwebsite, Sidi}

Interestingly enough, although we require that \(A(y)\) have an asymptotic series expansion,\cite{NumericalAnalysis}
we are only curious about its form. Specifically, we will examine functions with expansions that
take this form: \\
\begin{equation}
\label{othequation}
A(h) = A +\sum_{k=1}^{s}
{\alpha}_k + h^{Pk} + O(h_{Ps+1})\hspace{1cm}\textrm{as}\hspace{0.5cm} {h\to 0_+} 
\end{equation}

\section{Simple Numerical Differentiation and Integration Schemes}
Before continuing with our development of the theory of extrapolation, we will briefly review
differentiation by first divided differences and quadrature using the (composite) trapezoid and
midpoint rules. These are among the simplest algorithms; they are straightforward enough
to be presented in first-year calculus classes. Being all first-order approximations, their error
modes are significant, and they have little application in real situations. However as we will
see, applying a simple extrapolation scheme makes these simple methods extremely powerful,
convering difficult problems quickly and achieving higher accuracy in the process.

\subsection{Differentiation via First Divided Differencs}
Suppose we have an arbitrary function \(f(x) {\in} C^1[x_0 - \epsilon, x_0 + \epsilon] \)for some \(\epsilon > 0\). Then apply a
simple linear fit to \(f(x)\) in the aforementioned region to approximate the \(f'(x)\). We can begin
by assuming the existence of additional derivatives and computing a Taylor Expansion:
\[f(x) = f(x_0) + f′(x_0)(x - x_0) + \frac{1}{2}f′′(x_0)(x - x_0)^2 + O(f(h^3))\]
where \(h = x - x_0\) and \(0 < h \leq \epsilon\)
\begin{equation}
\label{2ndequation}
f′(x_0) = \frac{f(x + h) − f(x)}{h} - \frac{1}{2} f′′(x)h + O(h^2)
\end{equation}
Implying that \(h_{optimum} \simeq \sqrt{eps} \) where eps is machine precision, which is roughly \(10^{-16}\) in IEEE
double precision. Thus our best error is \(4(f''(x)f(x)\epsilon)^{\frac{1}{2}} \simeq 10^{-8}\). This is rather dismal, and we
certainly can do better. Naturally it is impossible to modify arithemtic error: it is a property
of double precision representations. So we look to the truncation error: a prime suspect for
extrapolation.

\begin{figure}[h]
\centering
\includegraphics[width=0.5\textwidth]{1}
\caption{Example of a parametric plot ($\sin (x), \cos(x), x$)}
\end{figure}

The following table presents a graphical representation of the extrapolation process. Notice
that the left-most column is the only column that involves actual function evaluations. The
second column represents estimates of I with the O(hp1) term eliminated using the algorithm
given above. The third column elmiminates O(hp2) and so forth. The actual approximations
that should be used for error checking can be found by reading off the entries on the main
diagonal of this matrix.
\begin{table}
\caption{Iteration of a Generic Extrapolation Method}
\label{table:1}
\centering
\begin{tabular}{c c c c c} 
\\
\hline
\(I_0(h)\) \\ 
\(I_0(\frac{h}{s})\) & \(I_1(h) \) \\
 \(I_0(\frac{h}{s_2})\) & \(I_1(\frac{h}{s})\) & \(I_2(h)\)\\
 \(I_0(\frac{h}{s_3})\) & \(I_1(\frac{h}{s_2})\) & \(I_2(\frac{h}{s})\) & \(I_3(h)\)\\
 \({\vdots}\) & \({\vdots}\) & \({\vdots}\) & \({\vdots}\) & \({\ddots}\)\\ [1ex] 
 \hline
\end{tabular}
\end{table}
\\

\section{The Richardson and Romberg Extrapolation Technique}
When implementing Richardson or Romberg extrapolation, it is most common to do it itera-
tively, unless storage is an extreme issue. Although the problem can be solved just as easily
using recursion, iteration is generally preferred. In an iterative method, storage of the full
table/matrix is not neccessary.
\begin{lstlisting}
%xs are the x-locations along the axis where the function is evaluated
%ys are the corresponding y-values
%p is the array of error powers
	n = length(xs);
	R = zeros(n); %create the table
	R(:,1) = ys;
	for j=1:n-1,
	   for i=1:(n-j),
	      R(i,j+1)=R(i,j)+(R(i,j)-R(i+1,j))/((xs(i+j)/xs(i))^(p(j)/j)-1);
	   end
	end
\end{lstlisting}

\begin{figure}[h]
\centering
\includegraphics[width=0.5\textwidth]{2}\caption{Trapezoid Rule applied to \(\int_{0.25}^{1.25} exp(-x^2)\)}
\label{fig:trap}
\end{figure}
\begin{table}
\caption{Slope between step-sizes}
\label{table:2}
\centering
\begin{tabular}{c}
\\
\hline

4.61680926238264\\
4.12392496687081\\
4.02980316622319\\
4.00738318535940\\
4.00184180151224\\
4.00045117971785\\
4.00034982057854\\
3.99697103596297\\
4.08671822443851\\
4.02553509210714\\
\hline
\end{tabular}
\end{table}\newpage
\[
\begin{split}
\frac{4trap(\frac{h}{2}) - trap(h)}{2^2 - 1}
 & = Simp(\frac{h}{2}) \\
 & =\frac{h}{3}\Bigg(f(a) + 2\sum_{j=1}^{n-1}f(x_j) + f(b)) - \frac{h}{6}(f(a)+2\sum_{j=1}^{n-1}f(x_j) + f(b)\Bigg) \\
 & =\frac{h}{3}\Bigg(f(a) + 2\sum_{j=1}^{\frac{n}{2}-1}f(x_j) + 4\sum_{j=1}^{\frac{n}{2}}f(x_j) +f(b)\Bigg)
\end{split}
\]
Thus Simpson’s Rule is:
\begin{equation}
\label{4thequation}
\textrm{Simpson’s Rule:}
\int_{a}^{b}f(x)/,dx = \frac{h}{3}\Bigg(f(a) + 2\sum_{j=1}^{\frac{n}{2}-1}f(x_j) + 4\sum_{j=1}^{\frac{n}{2}}f(x_j) +f(b)\Bigg) + O(h^4)
\end{equation}
\section{Conclusion}
Extrapolation allows us to achieve greater precision with fewer function evaluations than un-
extrapolated methods. In some cases (as we saw with simple difference schemes), extrapolation
is the only way to improve numeric behavior using the same algorithm (i.e. without switching
to centered differences or something better). Using sets of poor estimates with small step-
sizes, we can easily and quickly eliminate error modes, reaching results that would be otherwise
unobtainable without significantly smaller step-sizes and function evaluations. Thus we turn
relatively poor numeric schemes like the Trapezoid Rule into excellent performers.

Of course the material presented here is certainly not the limit of extrapolation methods.
The Richardson and Romberg techniques are part of a fundamental set of methods using
polynomial approximations. There exist other methods using rational function approximations
(e.g. Pade, Burlirsch Stoer) along with generalizations of material presented here. For example,
Sidi gives detailed analysis of extrapolating functions whose expansions are more complicated
than Figure \ref{fig:trap} and Table \ref{table:2}

\begin{multicols}{2}
Extrapolation allows us to achieve greater precision with fewer function evaluations than un-
extrapolated methods. In some cases (as we saw with simple difference schemes), extrapolation
is the only way to improve numeric behavior using the same algorithm (i.e. without switching
to centered differences or something better). Using sets of poor estimates with small step-
sizes, we can easily and quickly eliminate error modes, reaching results that would be otherwise
unobtainable without significantly smaller step-sizes and function evaluations. Thus we turn
relatively poor numeric schemes like the Trapezoid Rule into excellent performers.
\end{multicols}

\begin{thebibliography}{9}
\bibitem{NumericalAnalysis} 
Burden, R. and D. Faires, 
\textit{Numerical Analysis} ,
8TH Ed, Thomson, 2005.
 
\bibitem{AnalysisofNumericalMethods} 
Issacson, E. and H. B. Keller.
\textit{Analysis of Numerical Methods},
Wiley and Sons. New York, 1966.
 
\bibitem{knuthwebsite} 
Press, et. al.
\textit{Numerical Recipes in C: The Art of Scientific Computing},
\url{http://www-cs-faculty.stanford.edu/uno/abcde.html},
Cambridge University Press, 1992.

\bibitem{Sidi} 
Sidi, Avram, 
\textit{Practical Extrapolation Methods}, Cambridge University Press, 2003.

\bibitem{stoer}
Stoer, J. and R. Burlirsch, 
\textit{Introduction to Numerical Analysis},
Springer-Verlag, 1980.
\end{thebibliography}

\end{document}

